all: help

help:
	cat README.md

install:
	echo `pwd`/ddiff.sh '$$@' > /usr/bin/ddiff

uninstall:
	rm /usr/bin/ddiff
